const puppeteer = require('puppeteer');


/**
 * This test is regarding controlling interent speed
 * Purpose -> Test Insurance Plan List Page Initial Filtering Issue
 */


// Diffrent network protocols
let NETWORK_PRESETS = {
    'GPRS': {
      'offline': false,
      'downloadThroughput': 50 * 1024 / 8,
      'uploadThroughput': 20 * 1024 / 8,
      'latency': 500
    },
    'Regular2G': {
      'offline': false,
      'downloadThroughput': 250 * 1024 / 8,
      'uploadThroughput': 50 * 1024 / 8,
      'latency': 300
    },
    'Good2G': {
      'offline': false,
      'downloadThroughput': 450 * 1024 / 8,
      'uploadThroughput': 150 * 1024 / 8,
      'latency': 150
    },
    'Regular3G': {
      'offline': false,
      'downloadThroughput': 750 * 1024 / 8,
      'uploadThroughput': 250 * 1024 / 8,
      'latency': 100
    },
    'Good3G': {
      'offline': false,
      'downloadThroughput': 1.5 * 1024 * 1024 / 8,
      'uploadThroughput': 750 * 1024 / 8,
      'latency': 40
    },
    'Regular4G': {
      'offline': false,
      'downloadThroughput': 4 * 1024 * 1024 / 8,
      'uploadThroughput': 3 * 1024 * 1024 / 8,
      'latency': 20
    },
    'DSL': {
      'offline': false,
      'downloadThroughput': 2 * 1024 * 1024 / 8,
      'uploadThroughput': 1 * 1024 * 1024 / 8,
      'latency': 5
    },
    'WiFi': {
      'offline': false,
      'downloadThroughput': 30 * 1024 * 1024 / 8,
      'uploadThroughput': 15 * 1024 * 1024 / 8,
      'latency': 2
    }
  }

  describe("Test Insurance Plan List Page Initial Filtering Issue", () => {
    test('Visit tab32 portal', async () => {
        // Launch headless Test
        browser = await puppeteer.launch({headless: false});
        page = await browser.newPage();
        await page.setViewport({ width: 1280, height: 800 })

        await page.goto('http://localhost:3000/patient');
        
        const navigationPromise = page.waitForNavigation();
        page.waitFor(3000);
        
        // Google Authentication
        await page.waitForSelector('.loginGoogleButtons')
        await page.click('.loginGoogleButtons')
    
        await navigationPromise
        await page.waitForSelector('input[type="email"]')
        await page.type('input[type="email"]', 'bharat.suryawanshi@tab32.com')
        await page.click('#identifierNext')
    
        await page.waitForSelector('input[type="password"]', { visible: true })
        await page.type('input[type="password"]','changeme')
    
        await page.waitForSelector('#passwordNext', { visible: true })
        await page.click('#passwordNext')
    
        await navigationPromise
          
        await page.waitFor(1000);
      }, 20000);
      
      test('select Clinic', async () => {
        await page.waitForSelector("[type='button']")
        const example = await page.$$("[type='button']");
        // console.log('example',example)
        await example[0].click();
      },10000);
      
      test('cancel Popup alert', async()=>{
        await page.waitFor(4000);
        await page.waitForSelector(".cancel");
        await page.click(".cancel");
        await page.waitFor(4000)
      }, 10000)
      
      test('Open More Options', async () => {
        await page.waitFor(2000);
        await page.waitForSelector("#clinicNavbar > li:nth-child(1) > a > span > i");
        await page.click("#clinicNavbar > li:nth-child(1) > a > span > i");
      })

      test('Open Settings', async () => {
        await page.waitFor(2000);
        await page.waitForSelector("#t32Onboard-SelectSetup > a > span > i");
        await page.click("#t32Onboard-SelectSetup > a > span > i");
      })

      
      test('Open Insurance Plan Section', async () => {
        await page.waitFor(2000);
        await page.waitForSelector('#t32Onboard-SelectSetup > ul.dropdown-menu', { visible: true})
        const stringIsIncluded = await page.evaluate(() => {
            const string = 'Setup Insurance Plan';
            const selector = '#t32Onboard-SelectSetup > ul.dropdown-menu';
            return document.querySelector(selector).innerText.includes(string);
        });
        expect(stringIsIncluded).toBeTruthy();
        
        await page.waitForSelector('#t32Onboard-SelectSetup > ul > li:nth-child(11)')
        
        // Connect to Chrome DevTools
        const client = await page.target().createCDPSession()

        // Set throttling property
        await client.send('Network.emulateNetworkConditions', NETWORK_PRESETS['Regular2G']);
        await page.click('#t32Onboard-SelectSetup > ul > li:nth-child(11)');

        await page.waitFor(15000);

        // // closing popup button
        // await page.waitForSelector('body > div.modal.fade.ng-isolate-scope.seventyFivePercent-Modal.in > div > div > div > t32-popup-closer > button')
        // await page.click('body > div.modal.fade.ng-isolate-scope.seventyFivePercent-Modal.in > div > div > div > t32-popup-closer > button');

        // await page.waitForSelector("#clinicNavbar > li:nth-child(1) > a > span > i");
        // await page.click("#clinicNavbar > li:nth-child(1) > a > span > i");
        
        // await page.waitForSelector("#t32Onboard-SelectSetup > a > span > i");
        // await page.click("#t32Onboard-SelectSetup > a > span > i");
        // await page.waitForSelector('#t32Onboard-SelectSetup > ul.dropdown-menu', { visible: true})
    
        // if(stringIsIncluded){
        //     await page.waitForSelector('#t32Onboard-SelectSetup > ul > li:nth-child(11)')
            
        //     // Connect to Chrome DevTools
        //     const client = await page.target().createCDPSession()

        //     // Set throttling property
        //     await client.send('Network.emulateNetworkConditions', NETWORK_PRESETS['GPRS']);
        //     await page.click('#t32Onboard-SelectSetup > ul > li:nth-child(11)');
            // await page.goto('https://www.youtube.com/watch?v=HxpjChVWQzY&list=RDHxpjChVWQzY&a=1');
        // }
        await browser.close();
      },100000)

  });