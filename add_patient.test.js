const puppeteer = require('puppeteer');
const {loginPart} = require('./login.test')
let page;
let browser;

/**
 * This test is part of add patient series
 * Test is on angular version 1 form
 * Purpose -> fill Mandatory fields and submit form (possitive)
 */


describe("Test Add Patient Section -> Part 1", () => {
  test("test Login part", async () => {

    try {
      browser = await puppeteer.launch({headless: false});
      page = await browser.newPage();
      // reusable code login
      await loginPart(page);          
    } catch (err) {
      console.log('There are some unexpected errors: ' + err);
    }
  
  },30000);


test('Select Create Patient section', async()=> {
    await page.waitForSelector("#bs-example-navbar-collapse-1 > form > create-patient-icon > a > span > i");
    await page.click("#bs-example-navbar-collapse-1 > form > create-patient-icon > a > span > i");
    await page.waitFor(1000)
});

test('check Save button disablility', async()=>{
    const isDisabled = await page.$eval('#onboardSavePatientButton', (button) => {
        return button.disabled;
    });
    expect(isDisabled).toBeTruthy();
});

test('check Custom fields button disablility', async()=>{
    const isDisabled = await page.$eval('body > div.modal.fade.ng-isolate-scope.in > div > div > div > form > div:nth-child(1) > div > div:nth-child(2) > div.col-md-4.text-left > button', (button) => {
        return button.disabled;
    });
    expect(isDisabled).toBeTruthy();
});

test('Fill Mandatory fields', async()=>{
    // First Name
    await page.waitForSelector('body > div.modal.fade.ng-isolate-scope.in > div > div > div > form > div:nth-child(1) > div > div:nth-child(4) > div:nth-child(2) > input')
    await page.type('body > div.modal.fade.ng-isolate-scope.in > div > div > div > form > div:nth-child(1) > div > div:nth-child(4) > div:nth-child(2) > input', 'Anna')

    //Last Name
    await page.type('body > div.modal.fade.ng-isolate-scope.in > div > div > div > form > div:nth-child(1) > div > div:nth-child(5) > div.col-md-7 > input', 'Sthesia')

    //DOB
    await page.waitForSelector('body > div.modal.fade.ng-isolate-scope.in > div > div > div > form > div:nth-child(1) > div > div:nth-child(6) > div:nth-child(2) > input')
    await page.click('body > div.modal.fade.ng-isolate-scope.in > div > div > div > form > div:nth-child(1) > div > div:nth-child(6) > div:nth-child(2) > input')
    
    await page.waitForSelector('body > div.modal.fade.ng-isolate-scope.in > div > div > div > form > div:nth-child(1) > div > div:nth-child(6) > div:nth-child(2) > div > table > tbody > tr:nth-child(2) > td:nth-child(4) > button')
    await page.click('body > div.modal.fade.ng-isolate-scope.in > div > div > div > form > div:nth-child(1) > div > div:nth-child(6) > div:nth-child(2) > div > table > tbody > tr:nth-child(2) > td:nth-child(4) > button')

    //Gender
    await page.select('body > div.modal.fade.ng-isolate-scope.in > div > div > div > form > div:nth-child(1) > div > div:nth-child(6) > div.col-md-2 > select', 'Male')

    //Email
    await page.type('body > div.modal.fade.ng-isolate-scope.in > div > div > div > form > div:nth-child(1) > div > div:nth-child(10) > div.col-md-7 > input', 'testing')

    //Cell
    await page.waitForSelector('body > div.modal.fade.ng-isolate-scope.in > div > div > div > form > div:nth-child(1) > div > div:nth-child(4) > div:nth-child(2) > input')
    await page.type('#cell', '1111111111')
});

test('Submit the form', async()=>{
    await page.click('#onboardSavePatientButton');

    await page.waitForSelector('#toast-container');

    await page.waitForSelector('#toast-container > div');

    const classNames = await page.$eval('#toast-container > div', el => {
      return $('#toast-container > div').attr('class');
    });

    // console.log('classNames',classNames);

    if(classNames.includes('toast-error')){
      fail('Error while saving the Patient Data')
    } else {
      expect(true).toBe(true);
    }
    
});

});
