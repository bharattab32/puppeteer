const puppeteer = require('puppeteer');
const {loginPart} = require('./login.test')
let page;
let browser;


/**
 * This test is part of add patient series
 * Test is on angular version 2 form
 * Purpose -> fill Mandatory fields, Test referral part and submit form (possitive)
 */


describe("Test Add Patient Section -> Part 2", () => {

  test("test Login part", async () => {
    try {
      browser = await puppeteer.launch({headless: false});
      page = await browser.newPage();
      // reusable code login
      await loginPart(page);          
    } catch (err) {
      console.log('There are some unexpected errors: ' + err);
    }
  },30000);

// test('Visit tab32 portal', async () => {
//   browser = await puppeteer.launch({headless: false});
//   page = await browser.newPage();
//   await page.setViewport({ width: 1280, height: 800 })
//   await page.goto('http://localhost:3000/patient');
//   const navigationPromise = page.waitForNavigation();
//   await page.waitForSelector('.loginGoogleButtons')
//     await page.click('.loginGoogleButtons')

//     await navigationPromise
//     await page.waitForSelector('input[type="email"]')
//     await page.type('input[type="email"]', 'bharat.suryawanshi@tab32.com')
//     await page.click('#identifierNext')

//     await page.waitForSelector('input[type="password"]', { visible: true })
//     await page.type('input[type="password"]','changeme')

//     await page.waitForSelector('#passwordNext', { visible: true })
//     await page.click('#passwordNext')

//     await navigationPromise

    
//   await page.waitFor(1000);
// }, 20000);

// test('select Clinic', async () => {
//   await page.waitForSelector("[type='button']")
//   const example = await page.$$("[type='button']");
//   // console.log('example',example)
//   await example[0].click();
// },15000);

// test('cancel Popup alert', async()=>{
//   await page.waitFor(4000);
//   await page.waitForSelector(".cancel");
//   await page.click(".cancel");
//   await page.waitFor(4000)
// }, 10000)


test('Select Create Patient section', async()=> {
    await page.waitForSelector("#bs-example-navbar-collapse-1 > form > create-patient-icon > a > span > i");
    await page.click("#bs-example-navbar-collapse-1 > form > create-patient-icon > a > span > i");
    await page.waitFor(1000)
});

test('check Save button disablility', async()=>{
    const isDisabled = await page.$eval('body > div.modal.fade.ng-isolate-scope.in > div > div > new-patient-creation > div > div:nth-child(6) > div > div > button:nth-child(1)', (button) => {
        return button.disabled;
    });
    expect(isDisabled).toBeTruthy();
});

test('check Custom fields button disablility', async()=>{
    const isDisabled = await page.$eval('body > div.modal.fade.ng-isolate-scope.in > div > div > new-patient-creation > div > form > div:nth-child(11) > div.col-xs-6.col-md-4.text-left.custom-button > button', (button) => {
        return button.disabled;
    });
    expect(isDisabled).toBeTruthy();
});

test('Fill Mandatory fields', async()=>{
    // First Name
    await page.waitForSelector('body > div.modal.fade.ng-isolate-scope.in > div > div > new-patient-creation > div > form > div:nth-child(1) > div:nth-child(1) > div > input')
    await page.type('body > div.modal.fade.ng-isolate-scope.in > div > div > new-patient-creation > div > form > div:nth-child(1) > div:nth-child(1) > div > input', 'Paige')

    //Last Name
    await page.type('body > div.modal.fade.ng-isolate-scope.in > div > div > new-patient-creation > div > form > div:nth-child(1) > div:nth-child(3) > div > input', 'Turner')

    //DOB
    await page.click('body > div.modal.fade.ng-isolate-scope.in > div > div > new-patient-creation > div > form > div:nth-child(2) > div:nth-child(1) > div > app-calendar > form > div > div > div > button')
    
    await page.waitForSelector('body > div.modal.fade.ng-isolate-scope.in > div > div > new-patient-creation > div > form > div:nth-child(2) > div:nth-child(1) > div > app-calendar > form > div > div > ngb-datepicker > div.ngb-dp-months > div > ngb-datepicker-month-view > div:nth-child(4) > div:nth-child(5) > div')
    await page.click('body > div.modal.fade.ng-isolate-scope.in > div > div > new-patient-creation > div > form > div:nth-child(2) > div:nth-child(1) > div > app-calendar > form > div > div > ngb-datepicker > div.ngb-dp-months > div > ngb-datepicker-month-view > div:nth-child(4) > div:nth-child(5) > div')

    //Gender
    await page.select('body > div.modal.fade.ng-isolate-scope.in > div > div > new-patient-creation > div > form > div:nth-child(2) > div:nth-child(2) > div > select', '1: 1')

    // state 
    await page.select('body > div.modal.fade.ng-isolate-scope.in > div > div > new-patient-creation > div > form > div:nth-child(5) > div:nth-child(2) > div > select', '7: CA')
    
    //Email
    await page.type('body > div.modal.fade.ng-isolate-scope.in > div > div > new-patient-creation > div > form > div:nth-child(7) > div:nth-child(1) > div > input', 'testing');
    await page.waitForSelector('body > div.modal.fade.ng-isolate-scope.in > div > div > new-patient-creation > div > form > div:nth-child(7) > div:nth-child(1) > div > div');
    const element = await page.$('body > div.modal.fade.ng-isolate-scope.in > div > div > new-patient-creation > div > form > div:nth-child(7) > div:nth-child(1) > div > div > div > span > i');
    const text = await page.evaluate(element => element.textContent, element);
    expect(text).toBe('Invalid Email.');

    await page.type('body > div.modal.fade.ng-isolate-scope.in > div > div > new-patient-creation > div > form > div:nth-child(7) > div:nth-child(1) > div > input', 'testing@gmail.com')

    //Cell
    await page.type('body > div.modal.fade.ng-isolate-scope.in > div > div > new-patient-creation > div > form > div:nth-child(7) > div:nth-child(2) > div > input', '1111111111')
}, 10000);

// add referral
test('add Referrals -> select existing patients from table', async()=>{
    // click on referral button
    await page.waitFor(2000);
    await page.click('body > div.modal.fade.ng-isolate-scope.in > div > div > new-patient-creation > div > form > div:nth-child(15) > div > h4 > span');
    await page.waitForSelector('body > div.modal.fade.ng-isolate-scope.in > div > div > new-patient-creation > div > form > div:nth-child(15) > div > div')

    // click on select button
    await page.click('body > div.modal.fade.ng-isolate-scope.in > div > div > new-patient-creation > div > form > div:nth-child(15) > div > div > div:nth-child(2) > div.col-sm-6 > app-patient-referrals > div > div > div > div.col-md-12 > button');
    await page.waitFor(2000);
    await page.waitForSelector('body > div.modal.fade.ng-isolate-scope.chartX_modal_window.in > div > div');
    const referralList = await page.$$('body > div.modal.fade.ng-isolate-scope.chartX_modal_window.in > div > div > div > table > tbody:nth-child(2) > tr > td:nth-child(8) > button', { visible: true });
    await referralList[0].click();
    await page.waitFor(1000)
}, 8000);

test('add Referrals -> add patient to referral', async()=>{
    await page.click('body > div.modal.fade.ng-isolate-scope.in > div > div > new-patient-creation > div > form > div:nth-child(15) > div > h4 > span');
    await page.waitForSelector('body > div.modal.fade.ng-isolate-scope.in > div > div > new-patient-creation > div > form > div:nth-child(15) > div > div')
    await page.waitFor(2000)
    await page.click('body > div.modal.fade.ng-isolate-scope.in > div > div > new-patient-creation > div > form > div:nth-child(15) > div > div > div:nth-child(3) > div.col-sm-6 > app-patient-referrals > div > div > div > div.col-md-12 > button');
    await page.waitForSelector('body > div.modal.fade.ng-isolate-scope.chartX_modal_window.in > div > div');
    await page.waitFor(2000);
    //click on add referral
    await page.click('body > div.modal.fade.ng-isolate-scope.chartX_modal_window.in > div > div > div > form > div > div > div > div.col-md-5.text-right > button')
    await page.waitFor(2000)
    // select referral type
    // await page.waitForSelector('body > div:nth-child(952) > div > div', {visible:true});
    // await page.click('body > div.modal.fade.ng-isolate-scope.in > div > div > div > form > div > div > div > select')
    await page.select('body > div.modal.fade.ng-isolate-scope.in > div > div > div > form > div > div > div > select', '0');
    
    // search patient
    await page.waitForSelector('body > div.modal.fade.ng-isolate-scope.in > div > div > div > form.form-horizontal.ng-dirty.ng-valid.ng-valid-required > div > div > div.col-md-5 > t32-patient-search > input.searchWidgetInput', {visible:true});
    await page.click('body > div.modal.fade.ng-isolate-scope.in > div > div > div > form.form-horizontal.ng-dirty.ng-valid.ng-valid-required > div > div > div.col-md-5 > t32-patient-search > input.searchWidgetInput');
    await page.type('body > div.modal.fade.ng-isolate-scope.in > div > div > div > form.form-horizontal.ng-dirty.ng-valid.ng-valid-required > div > div > div.col-md-5 > t32-patient-search > input.searchWidgetInput', 'family', {delay: 500});
    await page.waitFor(1000);
    const stringIsIncluded = await page.evaluate(() => {
        const string = '1 Family';
        const selector = 'body > div.modal.fade.ng-isolate-scope.in > div > div > div > form.form-horizontal.ng-dirty.ng-valid.ng-valid-required > div > div > div.col-md-5 > t32-patient-search > ul.dropdown-menu';
        return document.querySelector(selector).innerText.includes(string);
    });
    expect(stringIsIncluded).toBeTruthy();
    await page.waitFor(2000);
    if(stringIsIncluded){
        await page.click('body > div.modal.fade.ng-isolate-scope.in > div > div > div > form.form-horizontal.ng-dirty.ng-valid.ng-valid-required > div > div > div.col-md-5 > t32-patient-search > ul.dropdown-menu > li:nth-child(2)')
    }
    
    await page.waitFor(2000)
    
    // click on save button
    await page.waitFor(2000);
    await page.waitForSelector('body > div.modal.fade.ng-isolate-scope.in > div > div > div > form.form-horizontal.ng-pristine.ng-valid.ng-valid-required > div.text-right > button.btn.btn-primary')
    await page.click('body > div.modal.fade.ng-isolate-scope.in > div > div > div > form.form-horizontal.ng-pristine.ng-valid.ng-valid-required > div.text-right > button.btn.btn-primary');
    
    await page.waitFor(2000);
    // Select patient just added
    const referralList = await page.$$('body > div.modal.fade.ng-isolate-scope.chartX_modal_window.in > div > div > div > table > tbody', { visible: true });
    const buttonEle = `body > div.modal.fade.ng-isolate-scope.chartX_modal_window.in > div > div > div > table > tbody:nth-child(${referralList.length+1}) > tr > td:nth-child(8) > button`
    await page.waitForSelector(`${buttonEle}`)

    await page.click(`${buttonEle}`)
}, 30000);

// test checkboxes
test('Selecting checkboxes', async()=>{

    const checkbox1 = await page.$('body > div.modal.fade.ng-isolate-scope.in > div > div > new-patient-creation > div > form > div:nth-child(13) > div:nth-child(2) > div > div > div:nth-child(1) > div > label > input');
    await checkbox1.click();

    const checkbox2 = await page.$('body > div.modal.fade.ng-isolate-scope.in > div > div > new-patient-creation > div > form > div:nth-child(13) > div:nth-child(2) > div > div > div:nth-child(2) > div > label > input');
    await checkbox2.click();

    const checkbox3 = await page.$('body > div.modal.fade.ng-isolate-scope.in > div > div > new-patient-creation > div > form > div:nth-child(13) > div:nth-child(2) > div > div > div:nth-child(3) > div > label > input');
    await checkbox3.click();

    await page.waitFor(1000);
    await checkbox1.click();
    await page.waitFor(1000);
    await checkbox2.click();
});



test('Submit the form', async()=>{
    await page.click('body > div.modal.fade.ng-isolate-scope.in > div > div > new-patient-creation > div > div:nth-child(6) > div > div');

    await page.waitForSelector('#toast-container');

    await page.waitForSelector('#toast-container > div');

    const classNames = await page.$eval('#toast-container > div', el => {
      return $('#toast-container > div').attr('class');
    });

    // console.log('classNames',classNames);

    if(classNames.includes('toast-error')){
      fail('Error while saving the Patient Data')
    } else {
      expect(true).toBe(true);
    }
    
});

});
