const puppeteer = require('puppeteer');
const {loginPart} = require('./login.test')
let page;
let browser;

/**
 * This test is part of add patient series
 * Test is on angular version 2 form
 * Purpose -> fill Mandatory fields, Test Email and cell Consent Preference part and submit form (possitive)
 */

describe("Test Add Patient Section -> Part 3", () => {

test("test Login part", async () => {
  try {
    browser = await puppeteer.launch({headless: false});
    page = await browser.newPage();
    // reusable code login
    await loginPart(page);          
  } catch (err) {
    console.log('There are some unexpected errors: ' + err);
  }
},30000);


test('Select Create Patient section', async()=> {
    await page.waitForSelector("#bs-example-navbar-collapse-1 > form > create-patient-icon > a > span > i");
    await page.click("#bs-example-navbar-collapse-1 > form > create-patient-icon > a > span > i");
    await page.waitFor(1000)
});

test('check Save button disablility', async()=>{
    const isDisabled = await page.$eval('body > div.modal.fade.ng-isolate-scope.in > div > div > new-patient-creation > div > div:nth-child(6) > div > div > button:nth-child(1)', (button) => {
        return button.disabled;
    });
    expect(isDisabled).toBeTruthy();
});

test('check Custom fields button disablility', async()=>{
    const isDisabled = await page.$eval('body > div.modal.fade.ng-isolate-scope.in > div > div > new-patient-creation > div > form > div:nth-child(11) > div.col-xs-6.col-md-4.text-left.custom-button > button', (button) => {
        return button.disabled;
    });
    expect(isDisabled).toBeTruthy();
});

test('check email and cell field are mandantory before unchecking checkbox', async()=>{
    const checkEmailMand = await page.$eval('body > div.modal.fade.ng-isolate-scope.in > div > div > new-patient-creation > div > form > div:nth-child(7) > div:nth-child(1) > div > label > sup',
    el=> $('body > div.modal.fade.ng-isolate-scope.in > div > div > new-patient-creation > div > form > div:nth-child(7) > div:nth-child(1) > div > label > sup').is(":hidden"));

    const checkTextMand = await page.$eval('body > div.modal.fade.ng-isolate-scope.in > div > div > new-patient-creation > div > form > div:nth-child(7) > div:nth-child(3) > div > label > sup',
    el=> $('body > div.modal.fade.ng-isolate-scope.in > div > div > new-patient-creation > div > form > div:nth-child(7) > div:nth-child(3) > div > label > sup').is(":hidden"));


    console.log('checkEmailMand', checkEmailMand, ' checkTextMand', checkTextMand)

})

test('Fill Mandatory fields', async()=>{
    // First Name
    await page.waitForSelector('body > div.modal.fade.ng-isolate-scope.in > div > div > new-patient-creation > div > form > div:nth-child(1) > div:nth-child(1) > div > input')
    await page.type('body > div.modal.fade.ng-isolate-scope.in > div > div > new-patient-creation > div > form > div:nth-child(1) > div:nth-child(1) > div > input', 'asad')

    //Last Name
    await page.type('body > div.modal.fade.ng-isolate-scope.in > div > div > new-patient-creation > div > form > div:nth-child(1) > div:nth-child(3) > div > input', 'shaikh')

    //DOB
    await page.click('body > div.modal.fade.ng-isolate-scope.in > div > div > new-patient-creation > div > form > div:nth-child(2) > div:nth-child(1) > div > app-calendar > form > div > div > div > button')
    
    await page.waitForSelector('body > div.modal.fade.ng-isolate-scope.in > div > div > new-patient-creation > div > form > div:nth-child(2) > div:nth-child(1) > div > app-calendar > form > div > div > ngb-datepicker > div.ngb-dp-months > div > ngb-datepicker-month-view > div:nth-child(4) > div:nth-child(5) > div')
    await page.click('body > div.modal.fade.ng-isolate-scope.in > div > div > new-patient-creation > div > form > div:nth-child(2) > div:nth-child(1) > div > app-calendar > form > div > div > ngb-datepicker > div.ngb-dp-months > div > ngb-datepicker-month-view > div:nth-child(4) > div:nth-child(5) > div')

    //Gender
    await page.select('body > div.modal.fade.ng-isolate-scope.in > div > div > new-patient-creation > div > form > div:nth-child(2) > div:nth-child(2) > div > select', '1: 1')

    // state 
    await page.select('body > div.modal.fade.ng-isolate-scope.in > div > div > new-patient-creation > div > form > div:nth-child(5) > div:nth-child(2) > div > select', '7: CA')
    
}, 10000);

test('check Consent Preference check-uncheck effects on email and cell field', async ()=>{
    
    await page.type('body > div.modal.fade.ng-isolate-scope.in > div > div > new-patient-creation > div > form > div:nth-child(7) > div:nth-child(3) > div > input', '9999999999')

    // email check box clicked
    const checkbox1 = await page.$('body > div.modal.fade.ng-isolate-scope.in > div > div > new-patient-creation > div > form > div:nth-child(13) > div:nth-child(3) > div > div > div:nth-child(1) > div > label > input');
    await checkbox1.click();


    const checkEmailMand = await page.$eval('body > div.modal.fade.ng-isolate-scope.in > div > div > new-patient-creation > div > form > div:nth-child(7) > div:nth-child(1) > div > label > sup',
    el=> $('body > div.modal.fade.ng-isolate-scope.in > div > div > new-patient-creation > div > form > div:nth-child(7) > div:nth-child(1) > div > label > sup').is(":hidden"));

    expect(checkEmailMand).toBeTruthy()

    //check save button disability
    const isDisabled = await page.$eval('body > div.modal.fade.ng-isolate-scope.in > div > div > new-patient-creation > div > div:nth-child(6) > div > div > button:nth-child(1)', (button) => {
        return button.disabled;
    });

    console.log('isDisabled=========>', isDisabled)
    expect(isDisabled).toBe(false);

    await page.type('body > div.modal.fade.ng-isolate-scope.in > div > div > new-patient-creation > div > form > div:nth-child(7) > div:nth-child(1) > div > input', 'testing');
    await page.waitForSelector('body > div.modal.fade.ng-isolate-scope.in > div > div > new-patient-creation > div > form > div:nth-child(7) > div:nth-child(1) > div > div');
    
    const element = await page.$('body > div.modal.fade.ng-isolate-scope.in > div > div > new-patient-creation > div > form > div:nth-child(7) > div:nth-child(1) > div > div > div > span > i');
    const text = await page.evaluate(element => element.textContent, element);
    console.log('text is=---?', text)
    expect(text).toBe('Invalid Email.');


    // clear text and try again
    await page.$eval('body > div.modal.fade.ng-isolate-scope.in > div > div > new-patient-creation > div > form > div:nth-child(7) > div:nth-child(1) > div > input', el => el.value = '');
    await page.type('body > div.modal.fade.ng-isolate-scope.in > div > div > new-patient-creation > div > form > div:nth-child(7) > div:nth-child(1) > div > input', 'after testing');
    await page.waitForSelector('body > div.modal.fade.ng-isolate-scope.in > div > div > new-patient-creation > div > form > div:nth-child(7) > div:nth-child(1) > div > div');
    
    const elementAfter = await page.$('body > div.modal.fade.ng-isolate-scope.in > div > div > new-patient-creation > div > form > div:nth-child(7) > div:nth-child(1) > div > div > div > span > i');
    const textAfter = await page.evaluate(element => element.textContent, elementAfter);
    console.log('text is=---?', textAfter)
    expect(textAfter).toBe('Invalid Email.');


    const checkbox2 = await page.$('body > div.modal.fade.ng-isolate-scope.in > div > div > new-patient-creation > div > form > div:nth-child(13) > div:nth-child(3) > div > div > div:nth-child(2) > div > label > input');
    await checkbox2.click();

    const checkTextMand = await page.$eval('body > div.modal.fade.ng-isolate-scope.in > div > div > new-patient-creation > div > form > div:nth-child(7) > div:nth-child(3) > div > label > sup',
    el=> $('body > div.modal.fade.ng-isolate-scope.in > div > div > new-patient-creation > div > form > div:nth-child(7) > div:nth-child(3) > div > label > sup').is(":hidden"));

    expect(checkTextMand).toBeTruthy();
    await page.click('body > div.modal.fade.ng-isolate-scope.in > div > div > new-patient-creation > div > form > div:nth-child(7) > div:nth-child(3) > div > input');
    await page.keyboard.press("Tab");

    await page.$eval('body > div.modal.fade.ng-isolate-scope.in > div > div > new-patient-creation > div > form > div:nth-child(7) > div:nth-child(1) > div > input', el => el.value = '');
    await page.type('body > div.modal.fade.ng-isolate-scope.in > div > div > new-patient-creation > div > form > div:nth-child(7) > div:nth-child(1) > div > input', 't@g.com');
    
    


})




test('Submit the form', async()=>{
    await page.click('body > div.modal.fade.ng-isolate-scope.in > div > div > new-patient-creation > div > div:nth-child(6) > div > div');

    await page.waitForSelector('#toast-container');

    await page.waitForSelector('#toast-container > div');

    const classNames = await page.$eval('#toast-container > div', el => {
      return $('#toast-container > div').attr('class');
    });

    // console.log('classNames',classNames);

    if(classNames.includes('toast-error')){
      fail('Error while saving the Patient Data')
    } else {
      expect(true).toBe(true);
    }
    
});

});
