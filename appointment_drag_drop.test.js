const puppeteer = require('puppeteer');
const {loginPart} = require('./login.test')

let page;
let browser;

/**
 * This test is to test appointement section
 * Purpose -> Test drag and drop operations
 * Note -> Unfinished
 */

describe("Test Add Appointment Section", () => {
  test("test Login part", async () => {
    try {
      browser = await puppeteer.launch({headless: false});
      page = await browser.newPage();
      // reusable code login
      await loginPart(page);          
    } catch (err) {
      console.log('There are some unexpected errors: ' + err);
    }
  },30000);

test('Select Scheduler Section', async()=> {
  await page.waitForSelector("#bs-example-navbar-collapse-1 > form > scheduler-nav-icon-modal > a > i.fa-stack-1x.navBgSecondaryColor.file-text");
  await page.click("#bs-example-navbar-collapse-1 > form > scheduler-nav-icon-modal > a > i.fa-stack-1x.navBgSecondaryColor.file-text");
  await page.waitFor(1000)
})

test('select Time slot', async()=> {
  await page.waitForSelector("#calendar-print-area > div.fc-view-container > div > table > tbody > tr > td > div > div > div.fc-slats > table > tbody > tr:nth-child(9) > td:nth-child(2)", { visible: true });
  // await page.click('#calendar-print-area > div.fc-view-container > div > table > tbody > tr > td > div > div > div.fc-slats > table > tbody > tr:nth-child(9) > td:nth-child(2)')
  const example = await page.$('#calendar-print-area > div.fc-view-container > div > table > tbody > tr > td > div > div > div.fc-slats > table > tbody > tr:nth-child(9) > td:nth-child(2)');
const bounding_box = await example.boundingBox();
console.log('bounding_box', bounding_box)
await page.mouse.move(70.40625, 256);
await page.mouse.down();
await page.mouse.move(126, 19);
await page.mouse.up();
}, 10000)
});