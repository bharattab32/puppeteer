const puppeteer = require('puppeteer');

let page;
let browser;

/**
 * This test is to test appointement section
 * Purpose -> To test search user section 
 */

describe("Test Add Appointment Section", () => {
test('Visit tab32 portal', async () => {
  browser = await puppeteer.launch({headless: false});
  page = await browser.newPage();
  await page.setViewport({ width: 1280, height: 800 })
  await page.goto('http://localhost:3000/patient');
  const navigationPromise = page.waitForNavigation();
  await page.waitForSelector('.loginGoogleButtons')
    await page.click('.loginGoogleButtons')

    await navigationPromise
    await page.waitForSelector('input[type="email"]')
    await page.type('input[type="email"]', 'bharat.suryawanshi@tab32.com')
    await page.click('#identifierNext')

    await page.waitForSelector('input[type="password"]', { visible: true })
    await page.type('input[type="password"]','changeme')

    await page.waitForSelector('#passwordNext', { visible: true })
    await page.click('#passwordNext')

    await navigationPromise

    
  await page.waitFor(1000);
}, 20000);

test('select Clinic', async () => {
  await page.waitForSelector("[type='button']")
  const example = await page.$$("[type='button']");
  // console.log('example',example)
  await example[0].click();
},10000);

test('cancel Popup alert', async()=>{
  await page.waitFor(4000);
  await page.waitForSelector(".cancel");
  await page.click(".cancel");
  await page.waitFor(4000)
}, 10000)

test('Select Appointment section', async()=> {
  await page.waitForSelector("#bs-example-navbar-collapse-1 > form > appointment-create-nav-icon-modal > a");
  await page.click("#bs-example-navbar-collapse-1 > form > appointment-create-nav-icon-modal");
  await page.waitFor(1000)
})

test('search Patient and select the searched one', async()=>{
  await page.waitForSelector("body > div.modal.fade.ng-isolate-scope.in > div > div > div > div.container-fluid > form > div > div > div:nth-child(2) > div:nth-child(2) > div.col-md-11 > div > div.col-md-8.text-right > t32-patient-search > input.searchWidgetInput", { visible: true });
  await page.click("body > div.modal.fade.ng-isolate-scope.in > div > div > div > div.container-fluid > form > div > div > div:nth-child(2) > div:nth-child(2) > div.col-md-11 > div > div.col-md-8.text-right > t32-patient-search > input.searchWidgetInput")
  await page.type('body > div.modal.fade.ng-isolate-scope.in > div > div > div > div.container-fluid > form > div > div > div:nth-child(2) > div:nth-child(2) > div.col-md-11 > div > div.col-md-8.text-right > t32-patient-search > input.searchWidgetInput', 'family', {delay: 100}); // Types slower, like a user
  await page.waitForSelector('body > div.modal.fade.ng-isolate-scope.in > div > div > div > div.container-fluid > form > div > div > div:nth-child(2) > div:nth-child(2) > div.col-md-11 > div > div.col-md-8.text-right > t32-patient-search > ul.dropdown-menu', { visible: true})
  const stringIsIncluded = await page.evaluate(() => {
    const string = '1 Family';
    const selector = 'body > div.modal.fade.ng-isolate-scope.in > div > div > div > div.container-fluid > form > div > div > div:nth-child(2) > div:nth-child(2) > div.col-md-11 > div > div.col-md-8.text-right > t32-patient-search > ul.dropdown-menu';
    return document.querySelector(selector).innerText.includes(string);
  });
  expect(stringIsIncluded).toBeTruthy();
  if(stringIsIncluded){
    await page.waitForSelector('body > div.modal.fade.ng-isolate-scope.in > div > div > div > div.container-fluid > form > div > div > div:nth-child(2) > div:nth-child(2) > div.col-md-11 > div > div.col-md-8.text-right > t32-patient-search > ul.dropdown-menu > li:nth-child(1)')
    await page.click('body > div.modal.fade.ng-isolate-scope.in > div > div > div > div.container-fluid > form > div > div > div:nth-child(2) > div:nth-child(2) > div.col-md-11 > div > div.col-md-8.text-right > t32-patient-search > ul.dropdown-menu > li:nth-child(2)')
  }
  await browser.close();
}, 10000)
});